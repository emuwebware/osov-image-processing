
**Commands**

`process_dir *input_dir* *filename_filter* *output_dir*`

`process *image1* *image2*`

**e.g.**

Process all files within the sample directory

` python process_dir.py "sample_dir/" "2017" "sample_dir/output/" `

Process two images that have a clear cavitation

` python process.py "samples/image_2.png" "samples/image_1.png" `

Process two images that are the same with no clear cavitation

` python process.py "samples/image_same_2.png" "samples/image_same_1.png" `