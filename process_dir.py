import base
import time, datetime
import sys
import getopt
import os

# python process_dir.py "sample_dir/" ".png" "sample_dir/output"
args = sys.argv
input_dir = args[1]
match_substring = args[2]
output_dir = args[3]
areas_file = None

# Processing options
bright_or_dark = "bright" #options: bright, dark, both (=first bright, then dark)
radius = 5

def main():

  global input_dir, match_substring, output_dir, areas_file

  # Find the relevant files
  matched_files = []
  for root, dirs, files in os.walk(input_dir):
    files_sorted = sorted(files);
    for i, name in enumerate(files_sorted):
      if match_substring in files_sorted[i]:
        # print files_sorted[i] + " matched";
        matched_files.append(files_sorted[i]);
      # else:
        # print files_sorted[i] + " not matched";
    break;
  
  if(len(matched_files) == 0):
    print "No files found"
    sys.exit()

  # Creat the output dir
  if not os.path.exists(output_dir):
    os.makedirs(output_dir)

  base.create_log(output_dir + "/log.txt");
  
  # Create areas files
  areas_file = open(output_dir + "/areas.csv", 'w')  
  areas_file.write("Image1,Image2,Processed File,Area\n");

  base.log("Median radius: " + str(radius))
  base.log("Bright or dark: " + bright_or_dark)
  base.log("Start time: " + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
  base.log("=================================================")

  # Process them
  for i, name in enumerate(matched_files):
    if(i > 0):
      print("Processing " + input_dir + matched_files[i] + " " + input_dir + matched_files[i-1]);
      base.log("Processing " + input_dir + matched_files[i] + " " + input_dir + matched_files[i-1]);
      
      output, area = process(input_dir + matched_files[i], input_dir + matched_files[i-1])
      output_filename = datetime.datetime.now().strftime('%Y%m%d%H%M%S') + ".png"
      
      base.write_image(output_dir + "/" + output_filename, output)
      
      csv_row = [input_dir + matched_files[i], input_dir + matched_files[i-1], output_filename, str(area)]
      areas_file.write(','.join(csv_row) + '\n')
      
      base.log("--")

  base.log("=================================================")
  base.log("End time: " + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
  base.close_log();

  exit();

  return

def process(filename_1, filename_2):

  global radius, bright_or_dark
  
  # Get the image difference
  base.start_timer()
  output = base.subtract_images(filename_1, filename_2)
  base.log("Difference completed: " + str(base.stop_timer_return_seconds()) + "s")

  # Remove outliers
  base.start_timer()
  output, blurred_image = base.median_filter_using_median_blur(bright_or_dark, radius, output)
  base.log("Outlier removal completed: " + str(base.stop_timer_return_seconds()) + "s")

  # Filter out pixels
  base.start_timer()
  output = base.filter_pixels(output, 5)
  base.log("Filtering completed: " + str(base.stop_timer_return_seconds()) + "s")

  # # Equalise the histogram (don't require this step)
  # base.start_timer()
  # output = base.equalize(output)
  # base.log("Equalizer completed: " + str(base.stop_timer_return_seconds()) + "s")
  
  # Apply threshold
  output = base.apply_threshold(output);
  base.log("Thresholding completed: " + str(base.stop_timer_return_seconds()) + "s")

  # Calculate the area
  area = len(output[output>0])
  base.log("Embolism total area: " + str(area))
  
  return output, area;



if __name__ == '__main__':

  try:
    main()
  except KeyboardInterrupt:
    base.log("Terminated by user");
    print("Terminated by user.")  
    base.close_log();    
    areas_file.close();



 
