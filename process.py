import base
import time, datetime
import sys
import getopt
import os

# python process.py "samples/image_2.png" "samples/image_1.png"
args = sys.argv
filename_1 = args[1]
filename_2 = args[2]

def main():

  global counter, log_file, timer_start_time
  global filename_1, filename_2
  
  timer_start_time = datetime.datetime.now()

  # Options
  bright_or_dark = "bright" #options: bright, dark, both (=first bright, then dark)
  radius = 5

  output_dir = "output/" + timer_start_time.strftime('%Y%m%d%H%M%S')
  os.makedirs(output_dir);

  base.create_log(output_dir + "/log.txt");

  # sample = cv2.imread("samples/sample10x10.png")
  # sample = cv2.imread("samples/sample2.png")
  # sample = cv2.cvtColor(sample, cv2.COLOR_BGR2GRAY)
  
  base.log("Median radius: " + str(radius))
  base.log("Bright or dark: " + bright_or_dark)
  base.log("Start time: " + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

  base.start_timer()

  # Get the image difference
  diff = base.subtract_images(filename_1, filename_2)
  base.write_image(output_dir + "/difference.png", diff)
  base.log("Difference completed: " + str(base.stop_timer_return_seconds()) + "s")

  base.start_timer()

  # Remove outliers
  output, blurred_image = base.median_filter_using_median_blur(bright_or_dark, radius, diff)
  # output = median_filter_using_median_blur(bright_or_dark, radius, diff)
  base.write_image(output_dir + "/median_blurred.png", blurred_image)
  base.write_image(output_dir + "/outliers_removed.png", output)
  base.log("Outlier removal completed: " + str(base.stop_timer_return_seconds()) + "s")
  
  base.start_timer()

  # Filter out pixels below threshold
  base.start_timer()
  output = base.filter_pixels(output, 5)
  base.write_image(output_dir + "/pixels_filtered.png", output)
  base.log("Filtering completed: " + str(base.stop_timer_return_seconds()) + "s")

  # # Equalise the histogram (don't seem to require this step)
  # output = base.equalize(output)
  # base.write_image(output_dir + "/histogram_equalised.png", output)
  # base.log("Equalizer completed: " + str(base.stop_timer_return_seconds()) + "s")

  # Apply threshold
  output = base.apply_threshold(output);
  base.write_image(output_dir + "/thresholded.png", output)
  
  base.log("Thresholding completed: " + str(base.stop_timer_return_seconds()) + "s")

  # Calculate the area
  area = len(output[output>0])
  base.log("Embolism total area: " + str(area))
  
  base.close_log()

  return

if __name__ == '__main__':
    main()



 
