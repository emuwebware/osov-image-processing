import time, datetime
import sys
import cv2
import numpy as np
import os

counter = 0
log_file = None
debug = False

# def detect_edges(pixels):
#     sobelX = cv2.Sobel(pixels, cv2.CV_16S, 1, 0)
#     sobelY = cv2.Sobel(pixels, cv2.CV_16S, 0, 1)
#     sobel = np.hypot(sobelX, sobelY)
#     sobel[sobel > 255] = 255

def create_log(path):
  global log_file
  log_file = open(path, 'a')  

def write_image(path, file):
  cv2.imwrite(path, file)

def close_log():
  global log_file
  log_file.close()

def log(entry):
  global log_file, debug
  log_file.write(str(entry) + '\n')
  if(debug):
    print(str(entry));

def apply_threshold(image):
  ret2,image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)  
  return image

def start_timer():
  global timer_start_time
  timer_start_time = datetime.datetime.now()

def stop_timer_return_seconds():
  global timer_start_time
  return (datetime.datetime.now() - timer_start_time).total_seconds()

def subtract_images(filename_1, filename_2):

  #read images and convert to 8 bit
  img_1 = cv2.imread(filename_1)
  img_2 = cv2.imread(filename_2)
  
  img_1 = cv2.cvtColor(img_1, cv2.COLOR_BGR2GRAY)
  img_2 = cv2.cvtColor(img_2, cv2.COLOR_BGR2GRAY)

  return cv2.subtract(img_1, img_2)

def filter_pixels(image, pixel_threshold):

  img_width = int(image.shape[1])
  img_height = int(image.shape[0])
  
  # hist_max = np.amax(image)

  # threshold = hist_max * filter_threshold
  filter_count = 0

  for y in range(0, img_height):
    for x in range(0, img_width):
      if image[y,x] < pixel_threshold:
        image[y,x] = 0
        filter_count += 1
  
  log("Filtered pixels: " + str(filter_count) + " (" + str(round((filter_count / (img_height * img_width)) * 100)) + "%)")

  return image

def equalize(image):
  
  img_width = int(image.shape[1])
  img_height = int(image.shape[0])

  hist_max = np.amax(image)
  hist_multiplier = 255 / hist_max

  # image = int(round(image * hist_multiplier))  

  for y in range(0, img_height):
    for x in range(0, img_width):
      image[y,x] = int(round(image[y,x] * hist_multiplier))  

  log("Hist max: " + str(hist_max) + " (" + str(hist_multiplier) + "), Final hist. max: " + str(np.amax(image)))

  return image

def median_filter_using_median_blur(bright_or_dark, radius, image):
  
  global counter, debug
  counter = 0

  output = image.copy()
  median_blurred_image = cv2.medianBlur(output, radius)

  img_width = int(image.shape[1])
  img_height = int(image.shape[0])

  median_radius_max = radius
  median_radius_min = radius * -1

  #run comparison loop
  for y in range(0, img_height):
    for x in range(0, img_width):
      
      surrounding_pixel_values = []
      
      surrounding_pixel_median = median_blurred_image[y,x]
        
      if(bright_or_dark=="dark" or bright_or_dark=="both"):
        if(surrounding_pixel_median >= image[y,x]):
          # log("Setting " + str(y) + ", " + str(x) + "(" + str(image[y,x]) + ") to surrounding pixel median (" + str(surrounding_pixel_median) + ")")
          output[y,x] = surrounding_pixel_median
          counter += 1
        
      if(bright_or_dark=="bright" or bright_or_dark=="both"):
        if(surrounding_pixel_median <= image[y,x]):
          # log("Setting " + str(y) + ", " + str(x) + "(" + str(image[y,x]) + ") to surrounding pixel median (" + str(surrounding_pixel_median) + ")")
          output[y,x] = surrounding_pixel_median
          counter += 1

  return output, median_blurred_image


def median_filter(bright_or_dark, radius, image):

  global counter, debug
  counter = 0

  output = image.copy()
  median_blurred_image = cv2.medianBlur(output, radius)

  img_width = int(image.shape[1])
  img_height = int(image.shape[0])

  median_radius_max = radius
  median_radius_min = radius * -1

  #run comparison loop
  for y in range(0, img_height):
    for x in range(0, img_width):
      
      surrounding_pixel_values = []
      
      for y_offset in range(median_radius_min, median_radius_max): # e.g. -5 to 5
        for x_offset in range(median_radius_min, median_radius_max):
          # skip pixels outside image dimensions
          if (y + y_offset < img_height and x + x_offset < img_width) and (y + y_offset >= 0 and x + x_offset >= 0): 
            surrounding_pixel_values.append(image[y + y_offset, x + x_offset])
      
      surrounding_pixel_manual_median = int(round(np.median(surrounding_pixel_values)))
      surrounding_pixel_blur_median = median_blurred_image[y,x];
      
      # log(str(surrounding_pixel_manual_median) + ", " + str(surrounding_pixel_blur_median));
      
      # if debug: print(y,x,surrounding_pixel_manual_median,image[y,x])
        
      if(bright_or_dark=="dark" or bright_or_dark=="both"):
        if(surrounding_pixel_manual_median >= image[y,x]):
          # log("Setting " + str(y) + ", " + str(x) + "(" + str(image[y,x]) + ") to surrounding pixel median (" + str(surrounding_pixel_manual_median) + ")")
          output[y,x] = surrounding_pixel_manual_median
          counter += 1
        
      if(bright_or_dark=="bright" or bright_or_dark=="both"):
        if(surrounding_pixel_manual_median <= image[y,x]):
          # log("Setting " + str(y) + ", " + str(x) + "(" + str(image[y,x]) + ") to surrounding pixel median (" + str(surrounding_pixel_manual_median) + ")")
          output[y,x] = surrounding_pixel_manual_median
          counter += 1

  return output